USE SuperheroesDb;

CREATE TABLE Superhero (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(255),
	Alias varchar(255),
	Origin varchar(255)
);

CREATE TABLE Assistant (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(255)
);

CREATE TABLE Power (
	Id int IDENTITY(1,1) PRIMARY KEY,
	Name varchar(255),
	Description varchar(1000)
);