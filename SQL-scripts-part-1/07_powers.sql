USE SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES ('Money', 'Vast amounts of money');

INSERT INTO Power (Name, Description)
VALUES ('Super Strength', 'Very strong');

INSERT INTO Power (Name, Description)
VALUES ('X-ray vision', 'Sees through objects');

INSERT INTO Power (Name, Description)
VALUES ('Thunder', 'Controls thunder');

INSERT INTO SuperheroPower (SuperheroId, PowerId)
VALUES (1,1);

INSERT INTO SuperheroPower (SuperheroId, PowerId)
VALUES (2,2);

INSERT INTO SuperheroPower (SuperheroId, PowerId)
VALUES (2,3);

INSERT INTO SuperheroPower (SuperheroId, PowerId)
VALUES (3,4);
