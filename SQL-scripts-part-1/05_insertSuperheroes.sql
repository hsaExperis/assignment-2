INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Bruce Wayne', 'Batman', 'Gotham City');

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Clark Kent', 'Superman', 'Smallville');

INSERT INTO Superhero (Name, Alias, Origin)
VALUES ('Thor', 'Thor', 'Asgaard');
