USE	SuperheroesDb;

CREATE TABLE SuperheroPower (
	SuperheroId int FOREIGN KEY REFERENCES Superhero(Id),
	PowerId int FOREIGN KEY REFERENCES Power(Id),
	CONSTRAINT SuperheroPowerId PRIMARY KEY (SuperheroId, PowerId)
);