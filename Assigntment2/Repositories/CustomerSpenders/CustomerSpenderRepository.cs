﻿using Assigntment2.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Repositories.CustomerSpenders
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        /// <summary>
        /// Gets the amount that each customer has spent and populates the dictionary in the Customerobject in descending order.
        /// </summary>
        /// <returns>CustomerSpender containing a dictionary</returns>/
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public CustomerSpender CustomerSpenders()
        {

            CustomerSpender customerSpender = new() {  HighestSpendingCustomers = new Dictionary<Customer, decimal>() };
            string sql = "SELECT Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email, SUM(Invoice.Total) as sumTotal FROM Customer " +
                "JOIN Invoice ON Invoice.CustomerId = Customer.CustomerId " +
                "GROUP BY Customer.CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email ORDER BY sumTotal DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                                customer.Email = !reader.IsDBNull(6) ? reader.GetString(6) : "";
                                decimal sumTotal = !reader.IsDBNull(7) ? reader.GetDecimal(7) : 0;
                                customerSpender.HighestSpendingCustomers.Add(customer, sumTotal);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerSpender;
        }
    }
}
