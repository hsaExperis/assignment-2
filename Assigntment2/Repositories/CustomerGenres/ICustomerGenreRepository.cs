﻿using Assigntment2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Repositories.CustomerGenres
{
    public interface ICustomerGenreRepository
    {
        public CustomerGenre GetCustomerGenres(Customer customer);
    }
}
