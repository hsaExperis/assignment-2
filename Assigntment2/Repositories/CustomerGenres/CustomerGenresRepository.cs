﻿using Assigntment2.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Repositories.CustomerGenres
{
    class CustomerGenresRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Gets the most popular genre (most bought) for a specific customer
        /// </summary>
        /// <param name="customer">The specific customer </param>
        /// <returns>CustomerGenre, contains a customer and a list of genres</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public CustomerGenre GetCustomerGenres(Customer customer)
        {
            CustomerGenre customerGenre = new();
            customerGenre.Customer = customer;
            customerGenre.Genres = new();
            string sql = "SELECT TOP 1 WITH TIES Genre.Name, COUNT(Track.GenreId) AS Number FROM Genre " +
                "JOIN Track ON Genre.GenreId = Track.GenreId " +
                "JOIN InvoiceLine ON InvoiceLine.TrackId = Track.TrackId " +
                "JOIN Invoice ON Invoice.InvoiceId = InvoiceLine.InvoiceId " +
                "WHERE Invoice.CustomerId = @CustomerId " +
                "GROUP BY Genre.Name " +
                "ORDER BY Number DESC";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {

                                customerGenre.Genres.Add(!reader.IsDBNull(0) ? reader.GetString(0) : "");

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }

            return customerGenre;
        }
    }
}
