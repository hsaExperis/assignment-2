﻿  using Assigntment2.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Repositories.CustomerCountries
{
    class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Gets the number of customers connected to each country.
        /// </summary>
        /// <returns>List of CustomerCountry objects</returns>
        /// /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public List<CustomerCountry> GetCustomerCountry()
        {
            List<CustomerCountry> nbrInContries = new();
            
            string sql = "SELECT Country, COUNT(CustomerId) AS nbrOfCustomer FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC;";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry tempCustomerCountry = new();
                                tempCustomerCountry.Country = !reader.IsDBNull(0) ? reader.GetString(0) : "";
                                tempCustomerCountry.NumberOfCustomers = !reader.IsDBNull(1) ? reader.GetInt32(1) : 0;
                                nbrInContries.Add(tempCustomerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return nbrInContries;
        }
    }
}
