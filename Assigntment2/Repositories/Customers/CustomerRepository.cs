﻿using Assigntment2.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Repositories.Customers
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Adds a new Customer object to the database.
        /// </summary>
        /// <param name="customer">The customer object to be added</param>
        /// <returns>returns True if succesful, otherwise False</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        /// <summary>
        /// Deletes a specific customer by id
        /// </summary>
        /// <param name="id">The id of the customer to be deleted</param>
        /// <returns>returns True if succesful, otherwise False</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool DeleteCustomer(int id)
        {
            bool success = false;
            string sql = "DELETE FROM Customer WHERE CustomerID = @CustomerID";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@CustomerID", id);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex);
            }
            return success;
        }

        /// <summary>
        /// Gets all the customers contained in the database and populates a list with the objects.
        /// </summary>
        /// <returns>A list of Customer object, contains all customers in the database</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customers = new List<Customer>();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                                customer.Email = !reader.IsDBNull(6) ? reader.GetString(6) : "";

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex);
            }

            return customers;
        }

        /// <summary>
        /// Gets a specific customer by id
        /// </summary>
        /// <param name="id">The id of the customer</param>
        /// <returns>The customer</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerID = @CustomerID";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                                customer.Email = !reader.IsDBNull(6) ? reader.GetString(6) : "";
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customer;
        }

        /// <summary>
        /// Gets a specific customer or several by querying the string and compares it to the customers first or last name and retrieves the ones that contain the parameter string.
        /// </summary>
        /// <param name="name">The name or a piece of the name of the customer to be retrieved</param>
        /// <returns>a List populated by Customer objects that correlates with the input parameter</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public List<Customer> GetCustomerByName(string name)
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE @FirstName OR LastName LIKE @LastName";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        if (name.Contains(" "))
                        {
                            string[] names = name.Split(" ");
                            command.Parameters.AddWithValue("@FirstName", "%" + names[0] + "%");
                            command.Parameters.AddWithValue("@LastName", "%" + names[1] + "%");
                        } 
                        else
                        {
                            command.Parameters.AddWithValue("@FirstName", "%" + name + "%");
                            command.Parameters.AddWithValue("@LastName", "%" + name + "%");
                        }
                        
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();

                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                                customer.Email = !reader.IsDBNull(6) ? reader.GetString(6) : "";

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customers;
        }

        /// <summary>
        /// Gets a page of customers delimited by limit and offset
        /// </summary>
        /// <param name="limit">How many customers on the page</param>
        /// <param name="offset">Where the page should begin</param>
        /// <returns>A page (List) of customers</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public List<Customer> GetCustomerPage(int limit, int offset)
        {
            List<Customer> customers = new List<Customer>();
            string sql = @"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@Offset", offset);
                        command.Parameters.AddWithValue("@Limit", limit);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = !reader.IsDBNull(1) ? reader.GetString(1) : "";
                                customer.LastName = !reader.IsDBNull(2) ? reader.GetString(2) : "";
                                customer.Country = !reader.IsDBNull(3) ? reader.GetString(3) : "";
                                customer.PostalCode = !reader.IsDBNull(4) ? reader.GetString(4) : "";
                                customer.Phone = !reader.IsDBNull(5) ? reader.GetString(5) : "";
                                customer.Email = !reader.IsDBNull(6) ? reader.GetString(6) : "";

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {

                Console.WriteLine(ex);
            }

            return customers;
        }

        /// <summary>
        /// Updates a customer
        /// </summary>
        /// <param name="customer">The customer to be updated</param>
        /// <returns>returns True if succesful, otherwise False</returns>
        /// <exception cref="SqlException"></exception>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="InvalidCastException"></exception>
        /// <exception cref="System.IO.IOException"></exception>
        /// <exception cref="ObjectDisposedException"></exception>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET " +
                "FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerID = @customerID";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    using (SqlCommand command = new SqlCommand(sql, conn))
                    {
                        command.Parameters.AddWithValue("@CustomerID", customer.CustomerId);
                        command.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success = command.ExecuteNonQuery() > 0 ? true : false;

                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }
    }
}
