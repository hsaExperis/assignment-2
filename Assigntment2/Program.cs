﻿using Assigntment2.Models;
using Assigntment2.Repositories.CustomerCountries;
using Assigntment2.Repositories;
using System;
using Assigntment2.Repositories.Customers;
using Assigntment2.Repositories.CustomerSpenders;
using Assigntment2.Repositories.CustomerGenres;

namespace Assigntment2
{
    class Program
    {


        public static string RequirementsDivider(int number)
        {
            return $"--------------------------------- Requirement: {number}---------------------------------";
        }

        public static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"{customer.FirstName} {customer.LastName}, {customer.Country}, {customer.PostalCode}, {customer.Phone}, {customer.Email}");
        }

        static void Main(string[] args)
        {

            // 1 Read all customers
            Console.WriteLine(RequirementsDivider(1));
            ICustomerRepository customerRepository = new CustomerRepository();
            foreach (Customer customerInAll in customerRepository.GetAllCustomers())
            {
                PrintCustomer(customerInAll);
            }
            Console.ReadLine();

            // 2 Read a specific customer by id
            Console.WriteLine(RequirementsDivider(2));
            Customer customer = customerRepository.GetCustomer(1);
            PrintCustomer(customer);
            Console.ReadLine();

            // 3 Read a specific customer by name
            Console.WriteLine(RequirementsDivider(3));
            foreach (Customer customerInByName in customerRepository.GetCustomerByName("lu"))
            {
                PrintCustomer(customerInByName);
            }
            Console.ReadLine();

            // 4 Return a page of customers (limit, offset)
            Console.WriteLine(RequirementsDivider(4));
            foreach (var customerInPage in customerRepository.GetCustomerPage(10, 10))
            {
                PrintCustomer(customerInPage);
            }
            Console.ReadLine();

            // 5 Add new customer
            Console.WriteLine(RequirementsDivider(5));
            Customer newCustomer = new Customer
            {
                FirstName = "Jane",
                LastName = "Doe",
                Country = "Sweden",
                PostalCode = "12345",
                Phone = "7654321",
                Email = "jane.doe@test.com"
            };
            Console.WriteLine(customerRepository.AddNewCustomer(newCustomer));
            Console.ReadLine();

            // 6 Update existing customer
            Console.WriteLine(RequirementsDivider(6));
            customer.FirstName = "New";
            Console.WriteLine(customerRepository.UpdateCustomer(customer));
            Console.ReadLine();

            // 7 Return number of in each country
            Console.WriteLine(RequirementsDivider(7));
            ICustomerCountryRepository customerCountryRepository = new CustomerCountryRepository();
            foreach (CustomerCountry customerCountry in customerCountryRepository.GetCustomerCountry())
            {
                Console.WriteLine($"{customerCountry.Country}: {customerCountry.NumberOfCustomers}");
            }
            Console.ReadLine();

            // 8 Return the highest spenders
            Console.WriteLine(RequirementsDivider(8));
            ICustomerSpenderRepository repository = new CustomerSpenderRepository();

            foreach (var item in repository.CustomerSpenders().HighestSpendingCustomers)
            {
                Console.WriteLine($"{item.Key.FirstName} {item.Value}");
            }
            Console.ReadLine();

            // 9 Return most popular genre for a specific customer
            Console.WriteLine(RequirementsDivider(9));
            Customer genreCustomer = customerRepository.GetCustomer(12);

            ICustomerGenreRepository genreRepository = new CustomerGenresRepository();

            Console.WriteLine($"{genreCustomer.FirstName} {genreCustomer.LastName}");
            foreach (string genre in genreRepository.GetCustomerGenres(genreCustomer).Genres)
            {
                Console.WriteLine(genre);
            }

            Console.ReadLine();
        }
    }
}
