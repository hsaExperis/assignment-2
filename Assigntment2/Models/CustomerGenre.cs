﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assigntment2.Models
{
    public class CustomerGenre
    {
        public Customer Customer { get; set; }
        public List<string> Genres { get; set; }
    }
}
